{ mode: -*- opascal -*- }

{$macro on}

{$define MOUSE_NOT_PRESSED:=((ciaa_pra^ and mousebit) <> 0)}

type
  AWord = UInt16;
  PAWord = ^AWord;

const
  ciaa_pra : ^UInt8 = Pointer($bfe001);
  zpos_reg : ^UInt8 = Pointer($dff006);
  col2_reg : ^UInt16 = Pointer($dff180);
  mousebit = (1 shl 6);
