program hwtest;

uses agraphics, hardware, intuition, sysutils;

{$i amiga.inc}

// BOOL init_display(void)
// {
//     LoadView(NULL);  // clear display, reset hardware registers
//     WaitTOF();       // 2 WaitTOFs to wait for 1. long frame and
//     WaitTOF();       // 2. short frame copper lists to finish (if interlaced)
//     return (((struct GfxBase *) GfxBase)->DisplayFlags & PAL) == PAL;
// }

const
  WB_BG_COL = $aaaa;
  Custom : pCustom = pCustom($dff000); { http://amigadev.elowar.com/read/ADCD_2.1/Hardware_Manual_guide/node0011.html }

procedure init_display;
begin
  LoadView(nil);
  { if you don't wait until the top of the frame before turning off the
    interrupts, then the LoadView() won't have chance to take effect }
  WaitTOF;
  WaitTOF;
end;

procedure reset_display;
begin
  LoadView(pGfxBase(GfxBase)^.ActiView);
  RethinkDisplay;
end;

var
  vpos : UInt8;
  incr : Int8;
  old_intena : Word;

begin

  // init_display;

  vpos := 200;
  incr := 1;

  { turn ints off, obvs if you don't turn them back on again you can't
    use the mouse }

  old_intena := Custom^.intenar;
  Custom^.intena := $7FFF or BITCLR;

  while MOUSE_NOT_PRESSED do begin

    while zpos_reg^ <> $ff do;

    inc(vpos, incr);

    if vpos = $f0 then incr := -1;
    if vpos = $40 then incr :=  1;

    while zpos_reg^ <> vpos do;
    col2_reg^ := $fff;
    while zpos_reg^ = vpos do;
    col2_reg^ := WB_BG_COL;
  end;

  { return the interrupts to OS state }
  Custom^.intena := old_intena or BITSET;

  // reset_display;

  // WriteLn(Format('%16X', [Custom^.intenar]));

end.
