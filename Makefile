FPC_OPTS=-Ooregvar -Pm68k -Tamiga
FPC=fpc

EXE=hwtest

all: $(EXE)

%: %.pp
	$(FPC) $(FPC_OPTS) $<

clean:
	rm -rv *.o $(EXE)
